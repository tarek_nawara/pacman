""""
This file contains implementation for depth
first search problem
> Created Date: 2017-11-22
"""

import util
from search import SearchProblem


def depth_first_search_impl(problem):
    """"

    Implementation of the depth first search algorithm.

    :param problem: search problem we are trying to solve
    :type problem SearchProblem
    :return list of actions
    """

    frontier = util.Stack()
    start = problem.getStartState()
    frontier.push((start, []))
    explored = set()

    while not frontier.isEmpty():
        (current, actions) = frontier.pop()
        explored.add(current)
        if problem.isGoalState(current):
            return actions

        for (neighbor, action, cost) in problem.getSuccessors(current):
            if neighbor not in explored:
                frontier.push((neighbor, actions + [action]))

    return []
