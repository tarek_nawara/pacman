""""
This file contains implementation for uniform
cost search problem
> Created Date: 2017-11-22
"""

from sets import Set

import util
from search import SearchProblem


def uniform_cost_search_impl(problem):
    """"

    Implementation of the uniform cost search algorithm.
    the priority assigned to the states in the heap are
    the total cost of the action to reach this state from
    the starting state.

   :param problem: search problem we are trying to solve
   :type problem SearchProblem
   :return list of actions
   """

    frontier = util.PriorityQueue()
    start = problem.getStartState()
    frontier.push((start, []), 0)
    explored = Set()

    while not frontier.isEmpty():
        (current, actions) = frontier.pop()
        if current in explored: continue
        explored.add(current)
        if problem.isGoalState(current):
            return actions

        for (neighbor, action, cost) in problem.getSuccessors(current):
            frontier.update((neighbor, actions + [action]), problem.getCostOfActions(actions + [action]))

    return []
