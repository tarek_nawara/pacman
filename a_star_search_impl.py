from sets import Set
from search import nullHeuristic
import util

def aStarSearchImpl(problem, heuristic):
    """"

    Implementation of the A* search algorithm.
    the priority assigned to the states in the heap are
    the total cost of the action to reach this state from
    the starting state + some predifined heuristic cost
	to reach nearest goal from this state(ex: this heuristic
	could be the manhattan distance between state and goal.)

   :param problem: search problem we are trying to solve
   :type problem SearchProblem
   :return list of actions
   """

    frontier = util.PriorityQueue()
    start = problem.getStartState()
    frontier.push((start, []), heuristic(start, problem))
    explored = []

    while not frontier.isEmpty():
        (current, actions) = frontier.pop()
        if current in explored: continue
        explored.append(current)
        if problem.isGoalState(current):
            return actions

        for (neighbor, action, cost) in problem.getSuccessors(current):
            frontier.update((neighbor, actions + [action]), problem.getCostOfActions(actions + [action])+heuristic(neighbor, problem))

    return []
