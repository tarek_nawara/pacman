""""
This file contains implementation for breadth
first search problem
> Created Date: 2017-11-22
"""

import util
from search import SearchProblem


def breadth_first_search_impl(problem):
    """"

    Implementation of the breadth first search algorithm

    :param problem: search problem we are trying to solve
    :type problem SearchProblem
    :return list of actions
    """

    frontier = util.Queue()
    start = problem.getStartState()
    frontier.push((start, []))
    explored = []

    while not frontier.isEmpty():
        (current, actions) = frontier.pop()
        explored.append(current)
        if problem.isGoalState(current):
            return actions

        for (neighbor, action, cost) in problem.getSuccessors(current):
            if (neighbor not in explored) and all(neighbor != s for (s, _) in frontier.list):
                frontier.push((neighbor, actions + [action]))

    return []
