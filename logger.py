class Logger:
    def __init__(self, file_name):
        self.file_name = file_name
        self.list = []
        self.mod = 'w'

    def info(self, s):
        with open(self.file_name, self.mod) as f:
            f.write(str(s))
            f.write('\n')
        self.mod = 'a'
